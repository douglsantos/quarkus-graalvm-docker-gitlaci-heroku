package com.douglasantos.products.configuration;

import javax.ws.rs.ext.ContextResolver;
import javax.ws.rs.ext.Provider;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;

@Provider
public class JacksonConfiguration implements ContextResolver<ObjectMapper> {
    
    @Override
    public ObjectMapper getContext(Class<?> type) {
        return new ObjectMapper()
        		.setPropertyNamingStrategy(PropertyNamingStrategy.SNAKE_CASE);
    }
}
