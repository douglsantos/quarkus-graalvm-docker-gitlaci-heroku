package com.douglasantos.products.entrypoints.model;

import java.util.UUID;

public class ProductModelResponseResource {

	private UUID idProduct;
	private String productName;
	private double unitPrice;

	public ProductModelResponseResource() {
	}

	public ProductModelResponseResource(UUID idProduct, String productName, double unitPrice) {
		this.idProduct = idProduct;
		this.productName = productName;
		this.unitPrice = unitPrice;
	}

	public UUID getIdProduct() {
		return idProduct;
	}

	public void setIdProduct(UUID idProduct) {
		this.idProduct = idProduct;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public double getUnitPrice() {
		return unitPrice;
	}

	public void setUnitPrice(double unitPrice) {
		this.unitPrice = unitPrice;
	}

}
