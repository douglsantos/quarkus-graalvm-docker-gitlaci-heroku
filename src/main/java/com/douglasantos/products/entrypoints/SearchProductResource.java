package com.douglasantos.products.entrypoints;

import java.util.UUID;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.douglasantos.products.entrypoints.model.ProductModelResponseResource;

@Path("/lemonade/v2/products")
@Produces(MediaType.APPLICATION_JSON)
public class SearchProductResource {
	
	@GET
	public Response searchProductByName() {
		
		
		return Response.ok(new ProductModelResponseResource(UUID.randomUUID(), "Xbox One X", 750.00))
				.build();
	}
}
